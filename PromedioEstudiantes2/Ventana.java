
import javax.swing.*; 
import java.awt.event.ActionEvent;  
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.util.*; 
import java.util.function.Supplier;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import javax.swing.DefaultRowSorter;
import javax.swing.RowFilter;
import javax.swing.SortOrder;
import javax.swing.table.TableRowSorter;
import javax.swing.table.TableModel;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Write a description of class Ventana here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Ventana extends JFrame
{
    public JPanel panel;
    public JTable table;
    public JTextField txt1;
    public JTextField txt2;
    public JTextField txt3;
    public JTextField txt4;
    public JTextField txt5;
    public DefaultTableModel tableModel = new DefaultTableModel();
    public JButton boton3;
    public JButton boton4;
    public List<Estudiante> estudiantes = new ArrayList<>();

    public TableRowSorter<TableModel> sorter;
    public Ventana()
    {
        setSize(500,500);// tamaño 
        setLocationRelativeTo(null);
        setTitle("Emilio  Romero");// titulo

        iniciarComponentes();
        setDefaultCloseOperation(EXIT_ON_CLOSE);

    }
    //se inicia todos los métodos
    private void iniciarComponentes()
    {
        crearPaneles();
        colocarEtiquetas();
        colocarBotones();
        cajaTexto();
        crearTabla();
    }
    //crea el panel
    private void crearPaneles()
    {
        panel=new JPanel();
        panel.setLayout(null);
        this.getContentPane().add(panel);
    }
    //crea las etiquetas 
    private void colocarEtiquetas()
    {
        JLabel etiquetaNombre= new JLabel();
        etiquetaNombre.setText("Estudiante: ");
        etiquetaNombre.setBounds(10,10,100,30);

        JLabel etiquetaCarnet= new JLabel();
        etiquetaCarnet.setText("Carnet: ");
        etiquetaCarnet.setBounds(250,10,100,30);

        JLabel etiquetaNotas= new JLabel();
        etiquetaNotas.setText("Nota: ");
        etiquetaNotas.setBounds(10,60,100,30);

        JLabel etiquetaPromedio = new JLabel();
        etiquetaPromedio.setText("Promedio minimo: ");
        etiquetaPromedio.setBounds(10,100,150,30);

        panel.add(etiquetaNombre);
        panel.add(etiquetaCarnet);
        panel.add(etiquetaNotas);
        panel.add(etiquetaPromedio);
    }
    //crea los botones 
    private void colocarBotones()
    {
        JButton boton1 = new JButton("Guardar");
        boton1.setBounds(250, 10, 90, 30);

        JButton boton2 = new JButton("Filtro");
        boton2.setBounds(280, 100, 90, 30);

        boton3 = new JButton("Guardar");
        boton3.setBounds(250, 60, 90, 30);

        boton4 = new JButton("Mayores");
        boton4.setBounds(10, 140, 100, 30);
        JButton boton5 = new JButton("Menores");
        boton5.setBounds(150, 140, 100, 30);
        JButton boton6 = new JButton("Iguales");
        boton6.setBounds(300, 140, 100, 30);
        //panel.add(boton1);
        //panel.add(boton2);
        panel.add(boton3);
        panel.add(boton4);
        panel.add(boton5);
        panel.add(boton6);
        ActionListener act = new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
                    txt1.getText();
                    txt2.getText();
                    txt3.getText();
                    Estudiante est = new Estudiante(txt1.getText(),txt2.getText(),Double.parseDouble(txt3.getText()));
                    tableModel.insertRow(0, new Object[] { est.getCarnet(),
                            est.getNombre(),
                            est.getPromedio() });
                    estudiantes.add(est);
                    txt1.setText(" ");
                    txt2.setText(" ");
                    txt3.setText(" ");
                    sortEstudiantes();
                }
            };
        boton3.addActionListener(act);
        ActionListener may = new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
                    mayores();
                }
            };
        boton4.addActionListener(may);
        ActionListener men = new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
                    menores();
                }
            };
        boton5.addActionListener(men);
        ActionListener igu = new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
                    iguales();
                }
            };
        boton6.addActionListener(igu);

    }

    private void sortEstudiantes () {
        List<RowSorter.SortKey> sortKeys = new ArrayList<>();

        sortKeys.add(new RowSorter.SortKey(2, SortOrder.ASCENDING));
        sorter.setSortKeys(sortKeys);
        sorter.sort();}
    // crea los txt
    private void cajaTexto()
    {
        txt1= new JTextField();
        txt1.setBounds(80, 10, 120, 30);

        txt2= new JTextField();
        txt2.setBounds(300, 10, 120, 30);

        txt3= new JTextField();
        txt3.setBounds(80, 60, 120, 30);

        txt4= new JTextField();
        txt4.setBounds(120,100,150,30);

        panel.add(txt1);
        panel.add(txt2);
        panel.add(txt3);
        panel.add(txt4);
    }
    //crea la tabla
    private void crearTabla () {

        table = new JTable(tableModel);
        tableModel.addColumn("carnet");
        tableModel.addColumn("nombre");
        tableModel.addColumn("promedio");

        panel.add(table);

        table.setBounds(20,180,400,250);

        table.addMouseListener(new MouseAdapter() {
                public void mouseClicked(MouseEvent me) {
                    // to detect doble click events
                    JTable target = (JTable)me.getSource();
                    int row = target.getSelectedRow(); // select a row
                    Object id = table.getValueAt(row, 0);
                    int column = target.getSelectedColumn(); // select a column
                    String nota = JOptionPane.showInputDialog(id);
                    Estudiante est = estudiantes.stream()
                        .filter(xEst -> xEst.getCarnet().equals(id))
                        .findAny()
                        .orElse(null);
                    est.agregarNota(Double.parseDouble(nota));
                    table.setValueAt(est.getPromedio(), row, 2);

                }
            });

        sorter = new TableRowSorter<>(table.getModel());
        table.setRowSorter(sorter);
        List<RowSorter.SortKey> sortKeys = new ArrayList<>();

        sortKeys.add(new RowSorter.SortKey(2, SortOrder.ASCENDING));
        sorter.setSortKeys(sortKeys);
        sorter.sort();

    }

    private void mayores () {

        while(tableModel.getRowCount() > 0)
        {
            tableModel.removeRow(0);
        }

        Stream<Estudiante> estFilter = estudiantes.stream();
        Stream<Estudiante> userResult = estFilter.filter(xEst -> xEst.getPromedio() > Double.parseDouble(txt4.getText()));
        userResult.forEach(xuser->tableModel.insertRow(0, new Object[] { xuser.getCarnet(),
                        xuser.getNombre(),
                        xuser.getPromedio() }));
        userResult.collect(Collectors.toList());

        sortEstudiantes();

    }

    private void menores () {

        while(tableModel.getRowCount() > 0)
        {
            tableModel.removeRow(0);
        }

        Stream<Estudiante> estFilter = estudiantes.stream();

        Stream<Estudiante> userResult = estFilter.filter(xEst -> xEst.getPromedio() < Double.parseDouble(txt4.getText()));

        userResult.forEach(xuser->tableModel.insertRow(0, new Object[] { xuser.getCarnet(),
                        xuser.getNombre(),
                        xuser.getPromedio() }));
        userResult.collect(Collectors.toList());

        sortEstudiantes();

    }

    private void iguales () {

        while(tableModel.getRowCount() > 0)
        {
            tableModel.removeRow(0);
        }

        Stream<Estudiante> estFilter = estudiantes.stream();

        Stream<Estudiante> userResult = estFilter.filter(xEst -> xEst.getPromedio() == Double.parseDouble(txt4.getText()));

        userResult.forEach(xuser->tableModel.insertRow(0, new Object[] { xuser.getCarnet(),
                        xuser.getNombre(),
                        xuser.getPromedio() }));
        List<Estudiante> students = userResult.collect(Collectors.toList());
        
       
      
        sortEstudiantes();
    }
}