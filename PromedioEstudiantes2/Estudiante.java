
/**
 * Write a description of class Estudiante here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.*; 
import java.util.function.Supplier;
public class Estudiante
{
    private String nombre;
    private String carnet;
    private Double nota;
    private List<Double> notas =new ArrayList<>();

    public Estudiante(String nombre,String carnet, Double nota) {
        this.nombre = nombre;
        this.nota = nota;
        this.carnet = carnet;
        this.notas.add(nota);
    }
    //se definen los getters y setters
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCarnet() {
        return carnet;
    }

    public void setCarnet(String carnet) {
        this.carnet = carnet;
    }

    public double getNota() {
        return nota;
    }

    public void setNota(double nota) {
        this.nota = nota;
    }

    public String toString() {
        return this.carnet + " - " + this.nombre+ " - " + this.nota ;
    }
    
    public void agregarNota(Double nota) {
        this.notas.add(nota);
    }
    //método para el promedio
    public Double getPromedio ()
    {
        Double sum = 0.0;
        if(!this.notas.isEmpty()) {
            for (Double xnota : this.notas) {
                sum += xnota;
            }
            return sum.doubleValue() / this.notas.size();
        }
        return sum; 
        
    }
}
     